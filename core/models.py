from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(blank=True, null=True, upload_to='images')
    date_created = models.DateTimeField(default=datetime.now)
    last_updated = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return "Title: " + self.title
