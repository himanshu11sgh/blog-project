from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import *

urlpatterns = [
    path('posts/create/', create_post, name='create post'),
    path('posts/<int:pk>/', get_single_post),
    path('posts/myposts/', get_my_posts, name='my posts'),
    path('posts/', get_all_posts, name='all posts'), 
    path('posts/edit/<int:pk>/', edit_post, name='edit post'),
    path('posts/delete/<int:pk>/', delete_post, name='delete post'),
    path('user/signup/', signup_user, name='signup'),
    path('user/login/', login_user, name='login'),
    path('user/logout/', logout_user, name='logout'),

] 

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)