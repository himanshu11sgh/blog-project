
from django.shortcuts import render, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core.files.storage import FileSystemStorage


from .forms import *

# Create your views here.


def get_my_posts_by_id(request):
    posts = Post.objects.all().filter(user=request.user)
    paginator = Paginator(posts, 8)
    page_number = request.GET.get('page')
    page_object = paginator.get_page(page_number)
    
    context = {
        'page_object': page_object,
        'title': 'My Posts', 
        'count': len(posts)
    }
    return context


def signup_user(request):
    if request.method=='POST':
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            messages.success(request, 'Your account is successfully created')
            form.save()
            return HttpResponseRedirect('/user/login/')
    form = SignUpForm()
    context = {'form': form}
    return render(request, 'user/signup_user.html', context)


def login_user(request):
    if request.method=='POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/posts/')

    form = AuthenticationForm()
    context = {'form': form}
    return render(request, 'user/login_user.html', context)


@login_required
def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/user/login/')


@csrf_exempt
@login_required
def create_post(request):
    if request.method == 'POST':
        post_id = request.POST.get('post-id')
        user = request.user.id
        title = request.POST.get('title')
        description = request.POST.get('description')
        dict = {
            'user': User.objects.get(id=user),
            'title': title,
            'description': description,
        }
        if post_id:
            if('image' in request.FILES):
                image = request.FILES['image']
                fss = FileSystemStorage()
                image_path = "images/" + image.name
                fss.save(image_path, image)
                dict['image'] = image_path
                Post.objects.filter(id=post_id).update(**dict)
            else:
                Post.objects.filter(id=post_id).update(**dict)
            return HttpResponseRedirect('/posts/' + post_id)
        else:
            form = PostForm(dict, request.FILES)
            if form.is_valid():
                form.save()
                context = get_my_posts_by_id(request)
                return render(request, 'post/all_posts.html', context)
    else:
        form = PostForm()
        context = {
            'form': form,
            'title': 'Create Post'
        }
        return render(request, 'post/createpost.html', context)


@login_required
def get_single_post(request, pk):
    page = Post.objects.get(id=pk)
    context = {'page': page, 'author': request.user.id}
    return render(request, 'post/single_post.html', context)
    

@login_required
def get_my_posts(request):
    context = get_my_posts_by_id(request)
    return render(request, 'post/all_posts.html', context)


@login_required
def get_all_posts(request):
    posts = Post.objects.all()
    paginator = Paginator(posts, 8)
    page_number = request.GET.get('page')
    page_object = paginator.get_page(page_number)

    context = {
        'page_object': page_object,
        'title': 'All Posts',
        'count': len(posts)
        }
    return render(request, 'post/all_posts.html', context)


@login_required
def edit_post(request, pk):
    if request.method == "GET":
        post = Post.objects.get(id=pk)
        if request.user.id == post.user.id:
            context = {
                'post': post,
                'title': 'Edit Post'
            }
            return render(request, 'post/createpost.html', context)
        else:
            return HttpResponseRedirect('/posts/myposts/')

@login_required
def delete_post(request, pk):
    post = Post.objects.get(id=pk)
    if request.user.id == post.user.id:
        post.delete()
    messages.info(request, 'You do not have permission')
    return HttpResponseRedirect('/posts/myposts/')

